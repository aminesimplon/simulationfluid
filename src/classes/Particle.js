import { c, canvas } from "../const"
import { Vector2 } from "./Vector2"

export class Particle{
    constructor(position, radius, deltaTime = 0.5){
        this.position = position
        this.radius = radius
        this.velocity = Vector2.zero
        this.acceleration = Vector2.zero
        this.gravity = new Vector2(0, 0.1)
        this.deltaTime = deltaTime
    }

    update(){
        console.log("position");
        console.log(this.position);
        this.draw()
        this.ApplyPhysics()
    }

    draw(){
        c.clearRect(0, 0, canvas.width, canvas.height);
        c.beginPath();
        c.strokeStyle = "white";
        c.lineWidth = 1;
        c.arc(this.position.x, this.position.y, this.radius, 0, Math.PI*2);
        c.fillStyle = "white";
        c.fill();
        c.stroke();
    
    }

    ApplyPhysics(){
        this.acceleration = this.acceleration.add(this.gravity);
        this.velocity = this.velocity.add(this.acceleration.mult(this.deltaTime));
        this.position = this.position.add(
            this.velocity.mult(this.deltaTime).add(
                this.acceleration.mult(
                    this.deltaTime * this.deltaTime)));
    }
}