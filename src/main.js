import { Particle } from "./classes/Particle";
import { Vector2 } from "./classes/Vector2";

const ball = new Particle(new Vector2(50, 50), 10, 0.1);

// loop()

function loop() {
    setTimeout(loop, 10);
    ball.update();
}

